# 更改数据节点名称

针对`alipay.story.find`接口，它的返回结果如下：

```json
{
	"alipay_story_find_response": {
		"msg": "Success",
		"code": "10000",
		"name": "白雪公主",
		"id": 1,
		"gmtCreate": 1554193987378
	},
	"sign": "xxxxx"
}
```

其中`alipay_story_find_response`是它的数据节点。规则是：

> 将接口名中的点`.`转换成下划线`_`，后面加上`_response`

代码实现如下：

```java
String method = "alipay.story.find";
return method.replace('.', '_') + "_response";
```

详见`DefaultDataNameBuilder.java`

如果要更改数据节点，比如`result`，可使用`CustomDataNameBuilder.java`。

```java
@Configuration
public class ZuulConfig extends AlipayZuulConfiguration {

    static {
        ...
        ApiConfig.getInstance().setDataNameBuilder(new CustomDataNameBuilder());
        ...
    }

}
```

设置后，网关统一的返回结果如下：

```json
{
	"result": {
		...
	},
	"sign": "xxxxx"
}
```

此外，构造方法可指定自定义字段名称：`new CustomDataNameBuilder("data");`。
设置后，数据节点将变成`data`

```json
{
	"data": {
		...
	},
	"sign": "xxxxx"
}
```

**注**：网关设置了CustomDataNameBuilder后，SDK也要做相应的更改：`SdkConfig.dataNameBuilder = new CustomDataNameBuilder();`
