package com.gitee.sop.gatewaycommon.bean;

/**
 * @author tanghc
 */
public interface BeanInitializer {
    void load();
}
